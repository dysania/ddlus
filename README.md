# ddlus

Just logs the weather for a given area.

Makes it a lot easier to answer the question: what was the weather that day?

## What's tha name from?
The name references [Daedalus](https://en.wikipedia.org/wiki/Daedalus), the father of [Icarus](https://en.wikipedia.org/wiki/Icarus) who flew too close to the sun.
